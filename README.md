# [Zadania - wzorce projektowe](#usage-overview)
Repozytorium zawiera rozwiązania zadań z ćwiczeń, które obejmują zagadnienia związane ze [wzorcami projektowowymi](https://pl.wikipedia.org/wiki/Wzorzec_projektowy_(informatyka)).

Jeżeli chcesz uzyskać kopię repozytorium Gita — zacząć wprowadzać własne zmiany
```c
git clone https://gitlab.com/mieloR/wzorce-projektowe.git
```

## Zawartość repozytorium
- [Singleton](#Singleton)
- [Dekorator](#Dekorator)
- [Fabryka](#Fabryka)
- [Fabryka z implementacją GUI](#Fabryka z implementacją interfejsu graficznego)
- [Memento](#Memento)
- [Adapter](#Adapter)
- [Chain of Responsibility](#Chain of Responsibility)
- [Proxy](#Proxy)
- [Kompozyt](#Kompozyt)

#### 1. Singleton   
> branch: wp-zad01

#### 2. Dekorator
> branch: wp-zad02

#### 3. Fabryka
> branch: wp-zad03

#### 4. Fabryka z implementacją interfejsu graficznego
 
##### 4.1. Frameworks and Tools
 
* Java&IDE: **JDK9  IntelliJ IDEA 2018 Ultimate**.  
* Backend: **SpringMVC Spring JDBC JPARepository Hibernate** (Konfiguracja poprzez adnotacje).
* Frontend: **CSS3 HTML5 Bootstrap jQuery-cookies Javascript JQuery Ajax** (Client-side i server-side komunikacja przez JSON).
* Database: **Oracle 11g**.
* Web Server: **Tomcat 8.5**.
* Build Tool: **Maven**.

> branch: wp-zad04

#### 5. Memento  

 > branch: wp-zad05

#### 6. Adapter  

 > branch: wp-zad06
 
#### 7. Chain of Responsibility

 > branch: wp-zad07
 
#### 8. Proxy

 > branch: wp-zad08
 
#### 9. Kompozyt

 > branch: wp-zad09
